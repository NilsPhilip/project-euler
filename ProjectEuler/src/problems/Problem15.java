package problems;

import java.math.BigInteger;

/**
 * Solution for Project Euler Problem 15.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
public class Problem15 {
	
	// Calculation for (N - 1) grid
	private final static int N = 21;
	private static BigInteger[][] numbers;
		
	public static void main(String[] args) {
		init(N);
		calc(N);
		System.out.println(get(N,N));
	}
	
	private static void init(int n) {
		numbers = new BigInteger[n][n];
		for (int i = 0; i < n; i++) {
			numbers[0][i] = BigInteger.ONE;
			numbers[i][0] = BigInteger.ONE;
		}
	}
	
	private static void set(int x, int y, BigInteger n) {
		numbers[x-1][y-1] = n;
	}
	
	private static BigInteger get(int x, int y) {
		return numbers[x-1][y-1];
	}
	
	private static void calc(int n) {
		for (int i = 2; i <= n; i++) {
			for (int j = 2; j <= n; j++) {
				set(i, j, get(i,j-1).add(get(i-1,j)));
			}
		}
	}
}
